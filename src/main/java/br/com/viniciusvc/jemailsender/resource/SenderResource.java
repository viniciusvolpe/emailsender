package br.com.viniciusvc.jemailsender.resource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import com.google.gson.Gson;

import br.com.viniciusvc.jemailsender.model.EmailModel;

@Path("/sender")
public class SenderResource {
	
	@POST
	public String send(String emailString){
		EmailModel email = new Gson().fromJson(emailString, EmailModel.class);
		SimpleEmail sender = new SimpleEmail();
		try {
			sender.setSmtpPort(465);
			sender.setSslSmtpPort("465");
			sender.setSSLOnConnect(true);
			sender.setStartTLSEnabled(true);
			sender.setSocketTimeout(10000);
			sender.setHostName("smtp.gmail.com");
			sender.setAuthenticator(new DefaultAuthenticator("jemailsender2015@gmail.com", "senderemailfromPedidos"));
			sender.addTo(email.getTo());
			sender.setFrom("viniciusvolpe44@gmail.com", "Pedido Online");
			sender.setSubject(email.getSubject());
			sender.setMsg(email.getBody());
			sender.send();
		} catch (EmailException e) {
			return e.getMessage();
		}
		return "enviado";
	}
}
