package br.com.viniciusvc.jemailsender.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import br.com.viniciusvc.jemailsender.resource.SenderResource;

@ApplicationPath("/rest")
public class JaxrsActivator extends Application {
	
	public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        s.add(SenderResource.class);
        return s;
    }
}
